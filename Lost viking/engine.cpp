#include "engine.hpp"
#include "cassert"
#include "stb_image.h"
#include <algorithm>
#include <iostream>

GLfloat deltaTime = 0.0f; // Время, прошедшее между последним
                          // и текущим кадром
GLfloat lastFrame = 0.0f; // Время вывода последнего кадра

Sprite::Sprite()
{
    vertices = {
        // Positions        // Texture Coords
        -1.0f, -1.0f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Top Right
        1.0f,  -1.0f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // Bottom Right
        1.0f,  1.0f,  -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // Bottom Left

        1.0f,  1.0f,  -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // Top Left
        -1.0f, 1.0f,  -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, // Top Right
        -1.0f, -1.0f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f  // Bottom Left
    };

    model = glm::mat4(1.0f);
};

Engine::Engine(int w, int h)
{
    screen_width  = w;
    screen_height = h;

    // camera
    lastX  = screen_width / 2;
    lastY  = screen_height / 2;
    camera = (glm::vec3(0.0f, 0.0f, 3.0f));
    //    camera.yaw = -180.0f;
    //    camera.pitch = -61.0f;
    camera.update_camera_vectors();

    // lighting
    lightPos = glm::vec3(.5f, 0.5f, 2.0f);
};

int Engine::init()
{
    using namespace std;
    stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };
    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);
    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        cerr << "warning: SDL2 compiled and linked version mismatch: "
             << &compiled << " " << &linked << endl;
        return 1;
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        cerr << "error: can't init: " << SDL_GetError() << endl;
        system("pause");
        return 1;
    }

    window = SDL_CreateWindow("Lost viking", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, screen_width,
                              screen_height, ::SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << "error: can't create window: " << SDL_GetError() << std::endl;
        system("pause");
        return 1;
    }

    int gl_major_ver = 3;
    int gl_minor_ver = 0;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context = SDL_GL_CreateContext(window);
    }

    assert(gl_context != nullptr);

    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    assert(result == 0);

    if (gl_major_ver < 3)
    {
        clog << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need openg version at least: 3.0\n"
             << flush;
        throw runtime_error("opengl version too low");
    }

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        cerr << "error: failed to initialize glad" << endl;
    }
    // ********* Build and compile our shader program ********* //
    guiShader = new Shader("shaders/material.vs", "shaders/gui.fs");
    lampShader     = new Shader("shaders/lamp.vs", "shaders/lamp.fs");
    print_opengl_version();

    // ====================
    // Setup OpenGL options
    // ====================
    glEnable(GL_DEPTH_TEST);
    // SDL_ShowCursor(SDL_FALSE); // HIDE CURSOR
    glEnable(GL_DEPTH_TEST);
       glEnable(GL_BLEND);
       glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // VAO and VBO
    glGenVertexArrays(1, &light_VAO);
    glGenBuffers(1, &light_VBO);

    glGenVertexArrays(1, &game_obj_VAO);
    glGenBuffers(1, &game_obj_VBO);

    glGenVertexArrays(1, &gui_VAO);
    glGenBuffers(1, &gui_VBO);

    validate_opengl_errors();
    return 0;
};

void Engine::swap_buffers()
{
    SDL_GL_SwapWindow(window);
    validate_opengl_errors();
}

void Engine::bind_sprites(Sprite& sprite)
{

    glBindVertexArray(game_obj_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, game_obj_VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(sprite.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &sprite.vertices.front(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);
    // Normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // Texture vertex attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid*>(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0); // game_obj_VAO

    // light's VAO and VBO
    glBindVertexArray(light_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, light_VBO);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<long>(sprite.vertices.size()) *
                     static_cast<long>(sizeof(GLfloat)),
                 &sprite.vertices.front(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          static_cast<GLvoid*>(nullptr));
    glEnableVertexAttribArray(0);

    glBindVertexArray(0); // Unbind light_VAO
};

unsigned int Engine::load_sprite_img(Sprite& sprite, const char* path)
{
    unsigned int textureID;
        glGenTextures(1, &textureID);

        int width, height, nrComponents;
        unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
        if (data)
        {
            GLenum format;
            if (nrComponents == 1)
                format = GL_RED;
            else if (nrComponents == 3)
                format = GL_RGB;
            else if (nrComponents == 4)
                format = GL_RGBA;

            glBindTexture(GL_TEXTURE_2D, textureID);
            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT); // for this tutorial: use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. Due to interpolation it takes texels from next repeat
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            stbi_image_free(data);
        }
        else
        {
            std::cout << "Texture failed to load at path: " << path << std::endl;
            stbi_image_free(data);
        }

        return textureID;
};

void Engine::draw_sprite(Sprite& sprite)
{
    // be sure to activate shader when setting uniforms/drawing objects
    guiShader->use();
    guiShader->setInt("material.diffuse", 0);
    guiShader->setInt("material.specular", 1);

    // lightingShader->setVec3("light.position", lightPos);
    guiShader->setVec3("light.direction", -0.2f, -1.0f, -0.3f);
    guiShader->setVec3("viewPos", camera.position);

    // light properties
    glm::vec3 lightColor;
    guiShader->setVec3("light.ambient", 0.2f, 0.2f, 0.2f);
    guiShader->setVec3("light.diffuse", 0.5f, 0.5f, 0.5f);
    guiShader->setVec3("light.specular", 1.0f, 1.0f, 1.0f);

    // material properties
    guiShader->setFloat("material.shininess", 64.0f);

    // view/projection transformations
    glm::mat4 view       = camera.GetViewMatrix();
    glm::mat4 projection = glm::perspective(
        camera.zoom, screen_width / static_cast<float>(screen_height), 0.1f,
        100.0f);
    //glm::mat4 projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, 0.1f, 100.0f );


    guiShader->setMat4("projection", projection);
    guiShader->setMat4("view", view);

    // world transformation
    glm::mat4 model = glm::mat4(1.0f);
    model           = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
    model           = glm::translate(model, glm::vec3(0.0f, 1.0f, -3.0f));
    guiShader->setMat4("model", model);

    // bind diffuse map
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, sprite.diffuseMap);
    // bind specular map
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, sprite.specularMap);

    // Draw the container (using container's vertex attributes)
    glBindVertexArray(game_obj_VAO);
    // render containers
    guiShader->setMat4("model", model);

     glDrawArrays(GL_TRIANGLES, 0, 36);

    glBindVertexArray(0);

};

int Engine::quit()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
    //    if (lightingShader != nullptr)
    //        delete lightingShader;
    //    if (lampShader != nullptr)
    //        delete lampShader;
    return 0;
};

void Engine::clear_display()
{
    glClearColor(.0f, .0f, .0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
};

Engine::~Engine()
{
    this->quit();
};

void Engine::validate_opengl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        std::string message;
        switch (error)
        {
            case GL_INVALID_ENUM:
                message =
                    "invalid enum passed to GL function (GL_INVALID_ENUM)";
                break;
            case GL_INVALID_VALUE:
                message = "invalid parameter passed to GL function "
                          "(GL_INVALID_VALUE)";
                break;
            case GL_INVALID_OPERATION:
                message = "cannot execute some of GL functions in current "
                          "state (GL_INVALID_OPERATION)";
                break;
            case GL_OUT_OF_MEMORY:
                message = "no enough memory to execute GL function "
                          "(GL_OUT_OF_MEMORY)";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                message = "invalid framebuffer operation "
                          "(GL_INVALID_FRAMEBUFFER_OPERATION)";
                break;
            default:
                message =
                    "error in some GL extension (framebuffers, shaders, etc)";
                break;
        }
        std::cerr << "OpenGL error: " << message << std::endl;
        std::abort();
    }
};

void Engine::print_opengl_version()
{
    using namespace std;
    string version     = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    string extentions_info =
        reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    clog << "OpenGL version: " << version << endl;
    clog << "OpenGL vendor: " << vendor_info << endl;
    // cerr << "Full OpenGL extention list: " << extentions_info << endl;
};

GLfloat Engine::calc_delta_time()
{
    GLfloat currentFrame = getime();
    deltaTime            = currentFrame - lastFrame;
    lastFrame            = currentFrame;
    return deltaTime;
}

// CAMERA
void Engine::read_input(bool& run)
{
    while (SDL_PollEvent(&e) != 0)
    {
        if (e.type == SDL_QUIT)
        {
            run = false;
        }

        // KEYDOWN
        if (e.type == SDL_KEYDOWN)
        {
            keys[e.key.keysym.sym] = true;
        }
        // KEYUP
        if (e.type == SDL_KEYUP)
        {
            keys[e.key.keysym.sym] = false;
        }
        //MOUSE CLICK DOWN
        if(e.type == SDL_MOUSEBUTTONDOWN){
            keys[e.button.button] = true;
        }
        //MOUSE CLICK UP
        if(e.type == SDL_MOUSEBUTTONUP){
            keys[e.button.button] = false;
        }
        // MOUSE WHEEL
        if (e.type == SDL_MOUSEWHEEL)
        {
            GLfloat offset = 1;
            if (e.wheel.y > 0) // scroll up
            {
                if (camera.zoom < 45)
                {
                    camera.zoom += offset;
                }
                else
                {
                    camera.zoom = 45;
                }
            }
            else if (e.wheel.y < 0) // scroll down
            {
                if (camera.zoom > 1)
                {
                    camera.zoom -= offset;
                }
                else
                {
                    camera.zoom = 1;
                }
            }
        }

        // MOUSE MOUTION
        if (e.type == SDL_MOUSEMOTION)
        {
            SDL_GetMouseState(&cursor_xpos, &cursor_ypos);
            if (first_mouse)
            {
                lastX       = cursor_xpos;
                lastY       = cursor_ypos;
                first_mouse = false;
            }

            GLfloat xoffset = cursor_xpos - lastX;
            GLfloat yoffset =
                lastY -
                cursor_ypos; // Reversed since y-coordinates go from bottom to left
            lastX = cursor_xpos;
            lastY = cursor_ypos;

            xoffset *= camera.mouse_sensitivity;
            yoffset *= camera.mouse_sensitivity;

            camera.yaw += xoffset;
            camera.pitch += yoffset;

            // Make sure that when pitch is out of bounds, screen doesn't get
            // flipped
            if (camera.pitch > 89.0f)
                camera.pitch = 89.0f;
            if (camera.pitch < -89.0f)
                camera.pitch = -89.0f;

            if(camera.camera_on){
            camera.update_camera_vectors();
            }
        }
    }
}

void Engine::do_movement(bool& run)
{
    float velocity = camera.movement_speed * deltaTime;
    if (keys[SDLK_ESCAPE])
        run = false;
    if (keys[SDLK_w])
    {
        camera.position += camera.front * velocity;
    }
    if (keys[SDLK_s])
    {
        camera.position -= camera.front * velocity;
    }
    if (keys[SDLK_a])
    {
        camera.position -= camera.right * velocity;
    }
    if (keys[SDLK_d])
    {
        camera.position += camera.right * velocity;
    }
    //Mouse click
    if(keys[SDL_BUTTON_LEFT]){
        std::cout << "mouse click" << std::endl;
    }
}
