#include "engine.hpp"
#include <iostream>

#include <cstdlib> // для использования exit()
#include <fstream>
#include <string>

using namespace std;

extern GLfloat lastFrame;
extern GLfloat deltaTime;


void printCameraPosition(Engine& engine){
    std::cout << "Camera position :" << endl;
    std::cout << "x = " << engine.camera.position.x <<
                 ", y = " << engine.camera.position.y <<
                 ", z = " << engine.camera.position.z << endl;
    std::cout << "----------------------------------------------" << endl;
    std::cout << "Camera yaw :" << engine.camera.yaw <<endl;
    std::cout << "Camera pitch :" << engine.camera.pitch <<endl;
}

void printCursorPosition(){

}

int main()
{
    Engine engine(800, 600);

    engine.init();

    Sprite button;
    engine.bind_sprites(button);
    // Load and create a texture Gothic-II-3-icon.png  container2.png
    button.diffuseMap = engine.load_sprite_img(button, "playbtn.png");
    button.specularMap = engine.load_sprite_img(button, "123.png");

    // ====================
    // GAME LOOP
    // ====================
    bool run = true;
    engine.camera.camera_on = false;
//    engine.lightingShader->use();
//        engine.lightingShader->setInt("material.diffuse", 0);
//        engine.lightingShader->setInt("material.specular", 1);

    while (run)
    {
        engine.calc_delta_time();
        engine.read_input(run);
        engine.do_movement(run);
        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

       //std::cout << "x:=" << engine.cursor_xpos << ", y:=" << engine.cursor_ypos <<endl;

        engine.draw_sprite(button);

        // Swap the screen buffers
        engine.swap_buffers();
    }

    engine.quit();

    return 0;
}

