cmake_minimum_required(VERSION 3.9)
project(sdlGameLoop)

add_executable(sdlGameLoop main.cpp)
target_compile_features(sdlGameLoop PUBLIC cxx_std_17)

#link with MY STATIC SDL
option(StaticDEBUGSDL2Lib "Do you want to use STATIC DEBUG SDL2?" OFF)
if(StaticDEBUGSDL2Lib)
    find_library(SDL libSDL2d.a REQUIRED)
    message("path of STATIC SDL2 debug: " ${SDL})
    target_include_directories(sdlGameLoop PRIVATE "/usr/local/include/SDL2/")
    target_link_libraries(sdlGameLoop PRIVATE
                                        ${SDL}
                                        -lm
                                        -ldl
                                        -lpthread
                                        -lrt)
endif()

#link with MY SHARED SDL
option(SharedDEBUGSDL2Lib "Do you want to use SHARED DEBUG SDL2?" ON)
if(SharedDEBUGSDL2Lib)
    find_library(SDL libSDL2d.so REQUIRED)
    message("path of SHARED SDL2 debug: " ${SDL})
    target_include_directories(sdlGameLoop PRIVATE "/usr/local/include/SDL2/")
    target_link_libraries(sdlGameLoop PRIVATE ${SDL})
endif()

#link with STATIC SDL from Repo
option(StaticSDL2Lib "Do you want to use STATIC SDL2?" OFF)
if(StaticSDL2Lib)
    find_library(SDL libSDL2.a REQUIRED)
    message("path of STATIC SDL2 debug: " ${SDL})
    target_include_directories(sdlGameLoop PRIVATE "/usr/include/SDL2/")
    target_link_libraries(sdlGameLoop PRIVATE
                              ${SDL}
                              -lm
                              -ldl
                              -lpthread
                              -lrt)
endif()

#link with SHARED SDL from Repo
option(SharedSDL2Lib "Do you want to use SHARED SDL2?" OFF)
if(SharedSDL2Lib)
    find_library(SDL libSDL2.so REQUIRED)
    message("path of SHARED SDL2: " ${SDL})
    target_include_directories(sdlGameLoop PRIVATE "/usr/include/SDL2/")
    target_link_libraries(sdlGameLoop PRIVATE ${SDL})
endif()

install(TARGETS ${PROJECT_NAME} 
        RUNTIME DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
        LIBRARY DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
        ARCHIVE DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests)
