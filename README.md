# GameDevCpp Projects

[![GD](https://bitbucket.org/Stanse/gamedevcpp/raw/f2ac5956ae5a87f523f484177661c0b989dc22f3/avatar.jpg)](https://bitbucket.org/Stanse/gamedevcpp/)

   
Build Platform   | Status (tests only)
---------------- | ----------------------
Linux x64        | ![Linux x64](https://img.shields.io/bitbucket/pipelines/stanse/gamedevcpp/master?style=for-the-badge)

It's a place for my home tasks from GameDevC++ course.

  1. Hello world like a professional :)
  2. Static and Shared libs
  3. ...

# Planed Features!

  - multi platform 2D Game(linux, windows, mac os, android, ios)
  - easy building with cmake
