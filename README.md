# GameDevCpp Projects

[![Cpp](avatar.jpg)](https://gitlab.com/Stanse/gamedevcpp)


Build Platform    | Status (tests only)
----------------- | -----------------------
Linux x64         | [![pipeline status](https://gitlab.com/Stanse/gamedevcpp/badges/master/pipeline.svg)](https://gitlab.com/Stanse/gamedevcpp/commits/master)

It's a place for my home tasks from GameDevC++ course.

  1.1 Hello world like a professional :)
  1.2 Static and Shared libs
  2. ...

# Planed Features!

  - multi platform 2D Game(linux, windows, mac os, android, ios)
  - easy building with cmake