cmake_minimum_required(VERSION 3.16)

project(Lost_Viking CXX C)

#********************* ENGINE *********************#
find_package(sdl2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})
file(GLOB SOIL_c "SOIL/*.c")

add_library(engine SHARED
    engine.cpp
    game_object.cpp
    glad/glad.c
    shader.hpp
    camera.hpp
    ${SOIL_c}
    )
target_compile_features(engine PUBLIC cxx_std_17)

find_package(sdl2 REQUIRED)

target_include_directories(engine PUBLIC
    #${SDL2_INCLUDE_DIRS}
    ${CMAKE_CURRENT_LIST_DIR}
    )
target_link_libraries(engine PUBLIC
    ${SDL2_LIBRARIES}
    #/usr/local/lib/libSDL2-2.0d.so
    )

target_link_libraries(engine PUBLIC
    -lSDL2
    -lGL
    )

add_executable(game main.cpp)
target_compile_features(game PUBLIC cxx_std_17)
target_link_libraries(game PUBLIC engine)

