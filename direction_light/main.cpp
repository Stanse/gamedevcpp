#include "engine.hpp"
#include <iostream>

#include <cstdlib> // для использования exit()
#include <fstream>
#include <string>

using namespace std;

extern GLfloat lastFrame;
extern GLfloat deltaTime;

int main()
{
    Engine engine(800, 600);

    engine.init();

    Game_object player;
    player.load_vertices("vert.txt");
    engine.bind_game_object(player);
    // Load and create a texture Gothic-II-3-icon.png  container2.png
    player.diffuseMap = engine.load_texture(player, "container2.png");
    player.specularMap = engine.load_texture(player, "container2_specular.png");

    // ====================
    // GAME LOOP
    // ====================
    bool run = true;
//    engine.lightingShader->use();
//        engine.lightingShader->setInt("material.diffuse", 0);
//        engine.lightingShader->setInt("material.specular", 1);

    while (run)
    {
        engine.calc_delta_time();
        engine.read_input(run);
        engine.do_movement(run);
        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // engine.clear_display();
//        std::cout << "Camera position :" << endl;
//        std::cout << "x = " << engine.camera.position.x <<
//                     ", y = " << engine.camera.position.y <<
//                     ", z = " << engine.camera.position.z << endl;
//        std::cout << "----------------------------------------------" << endl;
//        std::cout << "Camera yaw :" << engine.camera.yaw <<endl;
//        std::cout << "Camera pitch :" << engine.camera.pitch <<endl;


        engine.draw(player);
    }

    engine.quit();

    return 0;
}
