#include "engine.hpp"
#include <iostream>

int main(int /*argc*/, char ** /*argv*/) {
  Engine engine(640, 480);
  engine.init();

  bool run{true};

  EngineEvent event;
  while (run) {

    while (engine.read_input(event)) {
      std::cout << event << std::endl;
      switch (event) {
      case EngineEvent::turn_off:
        run = false;
        break;
      default:
        break;
      }
    }

    engine.clear();
    engine.swap_buffers();
  }
  return 0;
}
