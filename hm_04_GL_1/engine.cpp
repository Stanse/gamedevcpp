#include "engine.hpp"

#define CHECK_OPENGL()                                                         \
  {                                                                            \
    const int err = static_cast<int>(glGetError());                            \
    if (err != GL_NO_ERROR) {                                                  \
      switch (err) {                                                           \
      case GL_INVALID_ENUM:                                                    \
        std::cerr << GL_INVALID_ENUM << std::endl;                             \
        break;                                                                 \
      case GL_INVALID_VALUE:                                                   \
        std::cerr << GL_INVALID_VALUE << std::endl;                            \
        break;                                                                 \
      case GL_INVALID_OPERATION:                                               \
        std::cerr << GL_INVALID_OPERATION << std::endl;                        \
        break;                                                                 \
      case GL_INVALID_FRAMEBUFFER_OPERATION:                                   \
        std::cerr << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;            \
        break;                                                                 \
      case GL_OUT_OF_MEMORY:                                                   \
        std::cerr << GL_OUT_OF_MEMORY << std::endl;                            \
        break;                                                                 \
      }                                                                        \
      assert(false);                                                           \
    }                                                                          \
  }

static std::array<std::string, 17> event_names = {
    {/// input events
     "left_pressed", "left_released", "right_pressed", "right_released",
     "up_pressed", "up_released", "down_pressed", "down_released",
     "select_pressed", "select_released", "start_pressed", "start_released",
     "button1_pressed", "button1_released", "button2_pressed",
     "button2_released",
     /// virtual console events
     "turn_off"}};

struct vertex {
  vertex() : x(0.f), y(0.f) {}
  float x;
  float y;
};

struct triangle {
  triangle() {
    v[0] = vertex();
    v[1] = vertex();
    v[2] = vertex();
  }
  vertex v[3];
};

std::ostream &operator<<(std::ostream &stream, const EngineEvent event) {
  std::uint32_t value = static_cast<std::uint32_t>(event);
  std::uint32_t minimal = static_cast<std::uint32_t>(EngineEvent::left_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(EngineEvent::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("too big event value");
  }
}

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
  out << static_cast<int>(v.major) << '.';
  out << static_cast<int>(v.minor) << '.';
  out << static_cast<int>(v.patch);
  return out;
}

std::istream &operator>>(std::istream &is, vertex &v) {
  is >> v.x;
  is >> v.y;
  return is;
}

std::istream &operator>>(std::istream &is, triangle &t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}

struct EngineBind {
  SDL_Keycode key;
  std::string_view name;
  EngineEvent event_pressed;
  EngineEvent event_released;
};

const std::array<EngineBind, 8> keys{
    {{SDLK_w, "up", EngineEvent::up_pressed, EngineEvent::up_released},
     {SDLK_a, "left", EngineEvent::left_pressed, EngineEvent::left_released},
     {SDLK_s, "down", EngineEvent::down_pressed, EngineEvent::down_released},
     {SDLK_d, "right", EngineEvent::right_pressed, EngineEvent::right_released},
     {SDLK_LCTRL, "button1", EngineEvent::button1_pressed,
      EngineEvent::button1_released},
     {SDLK_SPACE, "button2", EngineEvent::button2_pressed,
      EngineEvent::button2_released},
     {SDLK_ESCAPE, "select", EngineEvent::select_pressed,
      EngineEvent::select_released},
     {SDLK_RETURN, "start", EngineEvent::start_pressed,
      EngineEvent::start_released}}};

static bool check_input(const SDL_Event &e, const EngineBind *&result) {
  using namespace std;

  const auto it = find_if(begin(keys), end(keys), [&](const EngineBind &b) {
    return b.key == e.key.keysym.sym;
  });

  if (it != end(keys)) {
    result = &(*it);
    return true;
  }
  return false;
}

Engine::Engine(int w, int h) {
  screen_width = w;
  screen_height = h;
};

int Engine::init() {
  using namespace std;
  stringstream serr;

  SDL_version compiled = {0, 0, 0};
  SDL_version linked = {0, 0, 0};
  SDL_VERSION(&compiled)
  SDL_GetVersion(&linked);
  if (SDL_COMPILEDVERSION !=
      SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
    cout << "warning: SDL2 compiled and linked version mismatch: " << &compiled
         << " " << &linked << endl;
    return 1;
  }

  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    cout << "error: can't init: " << SDL_GetError() << endl;
    system("pause");
    return 1;
  }

  window = SDL_CreateWindow("OpenGL_1", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, screen_width,
                            screen_height, ::SDL_WINDOW_OPENGL);
  if (window == nullptr) {
    std::cout << "error: can't create window: " << SDL_GetError() << std::endl;
    system("pause");
    return 1;
  }

  int gl_major_ver = 2;
  int gl_minor_ver = 0;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (gl_context == nullptr) {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    gl_context = SDL_GL_CreateContext(window);
  }

  assert(gl_context != nullptr);

  int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);

  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 2) {
    std::clog << "current context opengl version: " << gl_major_ver << '.'
              << gl_minor_ver << '\n'
              << "need openg version at least: 2.1\n"
              << std::flush;
    throw std::runtime_error("opengl version too low");
  }

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
    std::clog << "error: failed to initialize glad" << std::endl;
  }

  //  SDL_Surface *screen_surface = SDL_GetWindowSurface(window);

  //  SDL_FillRect(screen_surface, nullptr,
  //               SDL_MapRGB(screen_surface->format, 0, 255, 0));
  //  SDL_UpdateWindowSurface(window);
  //  SDL_Delay(2000);

  //  glClearColor(0.f, 1.0, 1.f, 0.0f);
  //  CHECK_OPENGL()
  //  glClear(GL_COLOR_BUFFER_BIT);
  //  CHECK_OPENGL()
  //  SDL_GL_SwapWindow(window);

  //  SDL_Delay(2000);

  return 0;
}

bool Engine::read_input(EngineEvent &e) {
  using namespace std;
  // collect all events from SDL
  SDL_Event sdl_event;
  if (SDL_PollEvent(&sdl_event)) {
    const EngineBind *binding = nullptr;

    if (sdl_event.type == SDL_QUIT) {
      e = EngineEvent::turn_off;
      return true;
    } else if (sdl_event.type == SDL_KEYDOWN) {
      if (check_input(sdl_event, binding)) {
        e = binding->event_pressed;
        return true;
      }
    } else if (sdl_event.type == SDL_KEYUP) {
      if (check_input(sdl_event, binding)) {
        e = binding->event_released;
        return true;
      }
    }
  }
  return false;
}

void Engine::swap_buffers() { SDL_GL_SwapWindow(window); };

int Engine::quit() {
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
};

void Engine::clear() {
  glClearColor(.5f, .25f, .25f, 0.0f);
  CHECK_OPENGL()
  glClear(GL_COLOR_BUFFER_BIT);
  CHECK_OPENGL()
};

Engine::~Engine() { this->quit(); }
