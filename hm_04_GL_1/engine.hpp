#ifndef ENGINE_HPP
#define ENGINE_HPP
#include "glad/glad.h"
#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <sstream>

#include <SDL.h>

struct vertex;
struct triangle;

enum class EngineEvent {
  /// input events
  left_pressed,
  left_released,
  right_pressed,
  right_released,
  up_pressed,
  up_released,
  down_pressed,
  down_released,
  select_pressed,
  select_released,
  start_pressed,
  start_released,
  button1_pressed,
  button1_released,
  button2_pressed,
  button2_released,
  /// virtual console events
  turn_off
};

std::ostream &operator<<(std::ostream &stream, const EngineEvent event);

std::istream &operator>>(std::istream &is, vertex &v);
std::istream &operator>>(std::istream &is, triangle &t);

class Engine {
public:
  Engine(int, int);
  ~Engine();
  int screen_width, screen_height;
  int init();
  bool read_input(EngineEvent &);
  int load();
  int quit();
  void clear();
  void swap_buffers();
  SDL_Window *window = nullptr;
};

#endif // ENGINE_HPP
