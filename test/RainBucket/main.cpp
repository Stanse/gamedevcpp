#include "engine.hpp"
#include <iostream>

using namespace std;

int main()
{
    Engine engine(800, 600);
    engine.init();
    engine.print_opengl_version();
    return 0;
}
