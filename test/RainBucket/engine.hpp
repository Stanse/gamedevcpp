#pragma once
#include "shader.hpp"
#include <SDL.h>

class Engine
{
public:
    Engine(int, int);
    ~Engine();

    int  init();
    void swap_buffers();
    void print_opengl_version();
    void validate_opengl_errors();
    int  quit();

    void set_screen_w(int w) { screen_width = w; }
    void set_screen_h(int h) { screen_height = h; }
    int  get_screen_w() { return screen_width; }
    int  get_screen_h() { return screen_height; }

    Shader* shader_prog = nullptr;

private:
    int           screen_width, screen_height;
    SDL_Window*   window     = nullptr;
    SDL_GLContext gl_context = nullptr;
};
