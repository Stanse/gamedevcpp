#include "engine.hpp"
#include "cassert"
#include <algorithm>

Engine::Engine(int w, int h)
{
    screen_width  = w;
    screen_height = h;
};

int Engine::init()
{
    using namespace std;
    stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };
    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);
    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        cerr << "warning: SDL2 compiled and linked version mismatch: "
             << &compiled << " " << &linked << endl;
        return 1;
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        cerr << "error: can't init: " << SDL_GetError() << endl;
        system("pause");
        return 1;
    }

    window = SDL_CreateWindow("Rain bucket", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, screen_width,
                              screen_height, ::SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << "error: can't create window: " << SDL_GetError() << std::endl;
        system("pause");
        return 1;
    }

    int gl_major_ver = 2;
    int gl_minor_ver = 0;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context = SDL_GL_CreateContext(window);
    }

    assert(gl_context != nullptr);

    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    assert(result == 0);

    if (gl_major_ver < 2)
    {
        clog << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need openg version at least: 2.1\n"
             << flush;
        throw runtime_error("opengl version too low");
    }

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    {
        cerr << "error: failed to initialize glad" << endl;
    }
    // ********* Build and compile our shader program ********* //
    shader_prog = new Shader("shader.vs", "shader.frag");

    return 0;
};

void Engine::swap_buffers()
{
    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/);
    validate_opengl_errors();
    SDL_GL_SwapWindow(window);
    validate_opengl_errors();
}

int Engine::quit()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
};

Engine::~Engine()
{
    this->quit();
};

void Engine::validate_opengl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        std::string message;
        switch (error)
        {
            case GL_INVALID_ENUM:
                message =
                    "invalid enum passed to GL function (GL_INVALID_ENUM)";
                break;
            case GL_INVALID_VALUE:
                message = "invalid parameter passed to GL function "
                          "(GL_INVALID_VALUE)";
                break;
            case GL_INVALID_OPERATION:
                message = "cannot execute some of GL functions in current "
                          "state (GL_INVALID_OPERATION)";
                break;
            case GL_OUT_OF_MEMORY:
                message = "no enough memory to execute GL function "
                          "(GL_OUT_OF_MEMORY)";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                message = "invalid framebuffer operation "
                          "(GL_INVALID_FRAMEBUFFER_OPERATION)";
                break;
            default:
                message =
                    "error in some GL extension (framebuffers, shaders, etc)";
                break;
        }
        std::cerr << "OpenGL error: " << message << std::endl;
        std::abort();
    }
};

void Engine::print_opengl_version()
{
    using namespace std;
    string version     = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    string extentions_info =
        reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    clog << "OpenGL version: " << version << endl;
    clog << "OpenGL vendor: " << vendor_info << endl;
    // cerr << "Full OpenGL extention list: " << extentions_info << endl;
};
