#include <exception>
#include <functional>
#include <iostream>
#include <vector>

//#include <glad/glad.h> // OpenGL ES 3.0
//#include <SDL_opengles2.h>
#include <SDL.h>

#include "SOIL/SOIL.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "shader.hpp"
#include "stb_image.h"

const int screen_width  = 800;
const int screen_height = 600;

int main(int /*argc*/, char* /*args*/[])
{
    // ********************** Initialize SDL2 ********************** //
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        return 1;
    }

    SDL_Surface* screen_surface = nullptr;
    SDL_Window*  window         = nullptr;

    window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen_width,
                              screen_height, ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        return 1;
    }

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0)
    { // Important!
        std::cout << "error: failed to initialize glad" << std::endl;
    }

    // ********* Build and compile our shader program ********* //
    Shader simpleShader("shader.vs", "shader.frag");
    Shader simpleDebugShader("shader.vs", "shaderDebug.frag");

    // Set up vertex data (and buffer(s)) and attribute pointers
    // clang-format off
//  GLfloat vertices[] = {
//      // Позиции              // Цвета            // Текстурные координаты
//         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   2.0f, 2.0f,   // Верхний правый
//         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   2.0f, 0.0f,   // Нижний правый
//        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // Нижний левый
//        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 2.0f    // Верхний левый
//       };

  float vertices[] = {
      -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
       0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
      -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

      -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
      -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
       0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
  };
  glm::vec3 cubePositions[] = {
    glm::vec3( 0.0f,  0.0f,  0.0f),
    glm::vec3( 2.0f,  5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3( 2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f,  3.0f, -7.5f),
    glm::vec3( 1.3f, -2.0f, -2.5f),
    glm::vec3( 1.5f,  2.0f, -2.5f),
    glm::vec3( 1.5f,  0.2f, -1.5f),
    glm::vec3(-1.3f,  1.0f, -1.5f)
  };
    // clang-format on
    GLuint indices[] = {
        // Note that we start from 0!
        0, 1, 3, // First Triangle
        1, 2, 3  // Second Triangle
    };

    // Setup OpenGL options
    glEnable(GL_DEPTH_TEST);

    GLuint VBO, VAO;
    // glad_glEnableVertexAttribArray(1);
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    //  GLuint EBO;
    // glGenBuffers(1, &EBO);

    // Bind the Vertex Array Object first, then bind and set vertex buffer(s)
    // and attribute pointer(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    //  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
    //               GL_STATIC_DRAW);

    // clang-format off
  // Position attribute
   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), static_cast<GLvoid*>(nullptr));
   glEnableVertexAttribArray(0);

   // Color attribute
   // glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof (GLfloat)));
   // glEnableVertexAttribArray(1);

   // Texture coord attribute
   glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof (GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof (GLfloat)));
   glEnableVertexAttribArray(2);
   //glBindVertexArray(0);

  //glBindBuffer(GL_ARRAY_BUFFER, 0);     // Note that this is allowed, the call to glVertexAttribPointer
                                        // registered VBO as the currently bound vertex buffer object so
                                        // afterwards we can safely unbind
    // clang-format on

    glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any
                          // buffer/array to prevent strange bugs)

    // ====================
    // Texture 1
    // ====================
    // clang-format off
  int width, height;
  unsigned char* image = SOIL_load_image("container.jpg", &width, &height, nullptr, SOIL_LOAD_RGB);
    GLuint texture;
    glGenTextures(1, &texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D,       // сообщили функции, что наша текстура привязана к этой цели (чтобы другие цели GL_TEXTURE_1D и GL_TEXTURE_3D не будут задействованы).
                  0,                   // аргумент описывает уровень мипмапа для которого мы хотим сгенерировать текстуру
                  GL_RGB,              // в каком формате мы хотим хранить текстуру
                  width, height,       // аргументы задают ширину и высоту результирующей текстуры, получили эти значения ранее во время загрузки изображения
                  0,                   // всегда должен быть 0. (Аргумент устарел).
                  GL_RGB,              // формат исходного изображения
                  GL_UNSIGNED_BYTE,    // тип данных исходного изображения
                  image                // данные изображения.
                  );
     glGenerateMipmap(GL_TEXTURE_2D);
    // clang-format on

    SOIL_free_image_data(image);

    // ====================
    // Texture 2
    // ====================
    // clang-format off
      image = SOIL_load_image("Gothic-II-3-icon.png", &width, &height, nullptr, SOIL_LOAD_RGB);
      GLuint texture1;
      glGenTextures(1, &texture1);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, texture1);

      glTexImage2D(GL_TEXTURE_2D,       // сообщили функции, что наша текстура привязана к этой цели (чтобы другие цели GL_TEXTURE_1D и GL_TEXTURE_3D не будут задействованы).
                    0,                   // аргумент описывает уровень мипмапа для которого мы хотим сгенерировать текстуру
                    GL_RGB,              // в каком формате мы хотим хранить текстуру
                    width, height,       // аргументы задают ширину и высоту результирующей текстуры, получили эти значения ранее во время загрузки изображения
                    0,                   // всегда должен быть 0. (Аргумент устарел).
                    GL_RGB,              // формат исходного изображения
                    GL_UNSIGNED_BYTE,    // тип данных исходного изображения
                    image                // данные изображения.
                    );
      glGenerateMipmap(GL_TEXTURE_2D);
    // clang-format on
    SOIL_free_image_data(image);

    // ====================
    // GAME LOOP
    // ====================
    bool      run = true;
    SDL_Event e;

    while (run)
    {
        while (SDL_PollEvent(&e) != 0)
        {
            if (e.type == SDL_QUIT)
            {
                run = false;
            }
        }
        glClearColor(0.2f, .3f, .3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Activate shader
        simpleShader.Use();

        // Create transformations
        // clang-format off
    //glm::mat4 model;
    glm::mat4 view;
    glm::mat4 projection;
//    GLfloat timeInSec = SDL_GetTicks() * 0.001f;
//    GLfloat angle = timeInSec * 50.0f;
//    model = glm::rotate(model, angle, glm::vec3(.5f, 1.0f, 0.0f));
    view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
    projection = glm::perspective(45.0f, screen_width / static_cast<float>(screen_height), 0.1f, 100.0f);
        // clang-format on

        // Bind Textures using texture units
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture1);
        glUniform1i(glGetUniformLocation(simpleShader.Program, "ourTexture1"),
                    0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(glGetUniformLocation(simpleShader.Program, "ourTexture2"),
                    1);

        // Get matrix's uniform location and set matrix
        // clang-format off
    GLint modelLoc = glGetUniformLocation(simpleShader.Program, "model");
    //glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

    GLint viewLoc = glGetUniformLocation(simpleShader.Program, "view");
    glad_glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

    GLint projectionLoc = glGetUniformLocation(simpleShader.Program, "projection");
    glUniformMatrix4fv(projectionLoc,                // позиция переменной
                       1,                           // кол-во матриц
                       GL_FALSE,                    // требуется транспонировать матрицу?
                       glm::value_ptr(projection)    // data
                       );

    GLint showDebugLoc =
    glGetUniformLocation(simpleShader.Program, "showDebug");
    glUniform1i(showDebugLoc, 0);
        // clang-format on
        // Draw
        glBindVertexArray(VAO);
        for (GLuint i = 0; i < 10; i++)
        {
            glm::mat4 model;
            model             = glm::translate(model, cubePositions[i]);
            GLfloat timeInSec = SDL_GetTicks() * 0.001f;
            GLfloat angle     = 30.0f * timeInSec * (i + 1.2);
            model = glm::rotate(model, angle, glm::vec3(.5f, 1.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
        glBindVertexArray(0);

        // Draw debug lines
        //    glBindVertexArray(VAO);
        //    glUniform1i(showDebugLoc, 1);
        //    glDrawArrays(GL_LINE_LOOP, 0, 36);
        //    glBindVertexArray(0);

        SDL_GL_SwapWindow(window);
        // SDL_Delay(40);
    }

    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
};
