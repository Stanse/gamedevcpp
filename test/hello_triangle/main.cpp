#include <exception>
#include <functional>
#include <iostream>
#include <vector>

//#include <glad/glad.h> // OpenGL ES 3.0
//#include <SDL_opengles2.h>
#include "shader.hpp"
#include "stb_image.h"
#include <SDL.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main(int /*argc*/, char * /*args*/[]) {

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    return 1;
  }

  SDL_Surface *screen_surface = nullptr;
  SDL_Window *window = nullptr;

  window =
      SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       SCREEN_WIDTH, SCREEN_HEIGHT, ::SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    return 1;
  }

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) { // Important!
    std::cout << "error: failed to initialize glad" << std::endl;
  }

  Shader simpleShader("shader.vs", "shader.frag");

  // Set up vertex data (and buffer(s)) and attribute pointers
  // clang-format off
  GLfloat vertices[] = {
      // Positions         // Colors
       0.5f, -0.5f, 0.0f,   1.0f, 0.0f, 0.0f, // Bottom Right
      -0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f, // Bottom Left
       0.0f, 0.5f,  0.0f,   0.0f, 0.0f, 1.0f  // Top
  };
  // clang-format on

  GLuint VBO, VAO;
  // glad_glEnableVertexAttribArray(1);
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and
  // attribute pointer(s).
  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // Position attribute
  // clang-format off
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), static_cast<GLvoid*>(nullptr));
  glEnableVertexAttribArray(0);

  // Color attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof (GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ARRAY_BUFFER, 0);     // Note that this is allowed, the call to glVertexAttribPointer
                                        // registered VBO as the currently bound vertex buffer object so
                                        // afterwards we can safely unbind
  // clang-format on

  glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any
                        // buffer/array to prevent strange bugs)

  bool run = true;
  SDL_Event e;

  while (run) {

    while (SDL_PollEvent(&e) != 0) {
      if (e.type == SDL_QUIT) {
        run = false;
      }
    }

    glClearColor(0.25f, 0.6f, .6f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw our first triangle
    simpleShader.Use();

    // Draw the triangle
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
    // SDL_Delay(20);
  }

  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
};
