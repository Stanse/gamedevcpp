//#include <stdlib.h>
#include <iostream>
#include <string>
#include "message.h"
#include "version.h"    // will generate
#include "path.h"       // will generate
static void cmdParse(const int argc, const char* argv[]);
static void printHelp(std::string aName);
static void printVersion();
static void printPath();
int main(int argc, char* argv[])
{
    if(argc > 1)
    {
        cmdParse(argc, const_cast<const char**>(argv));
    }

    message mes;
    mes.printMessage();
    bool is_good = std::cout.good();
    return is_good ? EXIT_SUCCESS : EXIT_FAILURE;
}
static void printHelp(std::string aName)
{
    std::cout
            <<"Usage: " << aName
            << " <option(s)>\n"
            <<"Options:\n"
            <<"\t-h, --help     Show help message \n"
            <<"\t-v, --version  Show version of this program \n"
            <<"\t-p, --path     Show path \n"
            <<"\t-g, --greeting Show 'Hello User' \n"
            <<std::endl;
}
static void cmdParse(const int argc, const char *argv[])
{
    for (int i = 1; i < argc ; i++ ) {
        std::string paramName = argv[i];
        if(paramName == "-h" || paramName == "--help")
        {
            printHelp(argv[0]);
            continue;
        }
        if(paramName == "-v" || paramName == "--version")
        {
            printVersion();
            continue;
        }
        if(paramName == "-p" || paramName == "--path")
        {
            printPath();
            continue;
        }
        if(paramName == "-g" || paramName == "--greeting")
        {
            const char* userEnvVar = std::getenv("USER");
            std::string userName = userEnvVar != nullptr ? userEnvVar : "USER";
            std::cout << "Hello, " << userName << std::endl;
            continue;
        }

    }
}
static void printVersion()
{
    std::cout << "Version: " << g_version << std::endl;
}
static void printPath()
{
    std::cout << "Programm path:\t" << g_source_path << std::endl;
    std::cout << "Binary path:\t" << g_binary_path << std::endl;
}