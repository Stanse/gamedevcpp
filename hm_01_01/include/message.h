#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>

struct message
{
    message(std::string n)
    {
        this->aName = n;
    }
    message()
    {
        this->aName = "World";
    }
    void printMessage();
    std::string aName;
};

#endif // MESSAGE_H
