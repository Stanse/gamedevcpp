cmake_minimum_required(VERSION 3.15)
project(02-sdl-static-debug)

file(GLOB
    CPPS "*.cxx")

add_executable(02-sdl-static-debug ${CPPS})
target_compile_features(02-sdl-static-debug PUBLIC cxx_std_17)

target_include_directories(02-sdl-static-debug PRIVATE "/usr/local/include/SDL2/")
find_library(SDL libSDL2d.a REQUIRED)
message("path == " ${SDL})
target_link_libraries(02-sdl-static-debug
               ${SDL}
               -lm
               -ldl
               -lpthread
               -lrt
               )


install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
    LIBRARY DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
    ARCHIVE DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests)
