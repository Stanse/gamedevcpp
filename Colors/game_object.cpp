#include "game_object.hpp"

#include <algorithm>
#include <cstdlib> // для использования exit()
#include <fstream>
#include <iostream>
#include <string>

Game_object::Game_object()
{
    vertices = {
        // positions             // normals           // texture coords
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f
    };
    model = glm::mat4(1.0f);
    glGenBuffers(1, &vbo);
}

void Game_object::rotate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z)
{
    model = glm::rotate(model, angle, glm::vec3(x, y, z));
};

void Game_object::translate(GLfloat x, GLfloat y, GLfloat z)
{
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(x, y, z));
};

void Game_object::scale(GLfloat x, GLfloat y, GLfloat z)
{
    model = glm::scale(model, glm::vec3(x, y, z));
};

void Game_object::scale(GLfloat scale)
{
    model = glm::scale(model, glm::vec3(scale, scale, scale));
}

int Game_object::load_vertices(const char* path)
{
    using namespace std;
    ifstream file;
    vertices.clear();
    GLfloat v;
    file.open(path, ios::binary | ios::in);
    if (!file)
    {
        cerr << "error: can't open " << path << endl;
        exit(1);
    }
    while (!file.eof())
    {
        file >> v;
        if (!file.eof())
            vertices.push_back(v);
    }
    file.close();
    return 0;
};
