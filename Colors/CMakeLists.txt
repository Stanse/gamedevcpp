cmake_minimum_required(VERSION 3.16)

project(Colors CXX C)

#********************* ENGINE *********************#
find_package(sdl2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})

add_library(engine SHARED
    engine.cpp
    game_object.cpp
    glad/glad.c
    shader.hpp
    camera.hpp
    )
target_compile_features(engine PUBLIC cxx_std_17)

find_package(sdl2 REQUIRED)

target_include_directories(engine PUBLIC
    #${SDL2_INCLUDE_DIRS}
    ${CMAKE_CURRENT_LIST_DIR}
    )
target_link_libraries(engine PUBLIC
    ${SDL2_LIBRARIES}
    #/usr/local/lib/libSDL2-2.0d.so
    )

target_link_libraries(engine PUBLIC
    -lSDL2
    -lGL
    -lSDL2_ttf
    -lSDL2_mixer
    )

add_executable(game main.cpp)
target_compile_features(game PUBLIC cxx_std_17)
target_link_libraries(game PUBLIC engine)

