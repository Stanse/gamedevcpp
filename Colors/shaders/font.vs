#version 300 es

layout (location = 0) in vec3 vert;
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 texcoord;

out vec4 fragColor;
out vec2 texCoord;

void main()
{
    fragColor = color;
    gl_Position = vec4(vert, 1);
    texCoord = texcoord;
}
